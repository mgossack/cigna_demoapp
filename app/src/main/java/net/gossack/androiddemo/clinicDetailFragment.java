package net.gossack.androiddemo;

import android.app.Activity;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.gossack.androiddemo.dummy.DummyContent;

/**
 * A fragment representing a single clinic detail screen.
 * This fragment is either contained in a {@link clinicListActivity}
 * in two-pane mode (on tablets) or a {@link clinicDetailActivity}
 * on handsets.
 */
public class clinicDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";
    public static final String ARG_CLINIC_NAME = "CLINIC_NAME";
    public static final String ARG_CLINIC_ADDR_STREET = "CLINIC_ADDR_STREET";
    public static final String ARG_CLINIC_ADDR_CITY = "CLINIC_ADDR_CITY";
    public static final String ARG_CLINIC_ADDR_STATE = "CLINIC_ADDR_STATE";
    public static final String ARG_CLINIC_ADDR_POSTAL_CODE = "CLINIC_ADDR_POSTAL_CODE";
    public static final String ARG_CLINIC_DISTANCE = "CLINIC_DISTANCE";
    public static final String ARG_CLINIC_PREFERRED = "CLINIC_PREFERRED";
    public static final String ARG_CLINIC_GEOLOC_LATITUDE = "CLINIC_LATITUDE";
    public static final String ARG_CLINIC_GEOLOC_LONGITUDE = "CLINIC_LONGITUDE";

    /**
     * The dummy content this fragment is presenting.
     */
    private DummyContent.DummyItem mItem;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public clinicDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            mItem = DummyContent.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));

            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(mItem.content);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.clinic_detail, container, false);

        // Show the dummy content as text in a TextView.
        if (mItem != null) {
            ((TextView) rootView.findViewById(R.id.clinic_detail)).setText(mItem.details);
        }

        return rootView;
    }
}
