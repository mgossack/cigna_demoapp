package net.gossack.androiddemo.adapter;


import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import net.gossack.androiddemo.R;
import net.gossack.androiddemo.clinicDetailActivity;
import net.gossack.androiddemo.clinicDetailFragment;
import net.gossack.androiddemo.model.ClinicContent;
import net.gossack.androiddemo.model.ClinicListContent;

/**
 * Created by Marc on 8/11/2016.
 */
public class ClientsAdapter extends RecyclerView.Adapter<ClientsAdapter.ViewHolder>
{
    ViewGroup _vgParent;
    private ClinicListContent _content;     // Clinic list from json file.
    Double _nearClinicDistance = 2.0;       // miles (would be calculation off current location) considered near if within
    Boolean _bTwoPane = true;

    private LayoutInflater inflater;

    public ClientsAdapter(ClinicListContent content) {
        super();
        this._content = content;
    }

    // Up to view
    public void setTwoPane(Boolean bTwoPane) {
        _bTwoPane = bTwoPane;
    }
    /**
     * setNearClinicDistance
     * @param distance - the distance within which a "closest" clinic view will be provided, otherwise
     *                 a normal view is provided.
     */
    public void setNearClinicDistance(Double distance) {
        _nearClinicDistance = distance;
    }
    public Double get_nearClinicDistance() {
        return _nearClinicDistance;
    }


    @Override
    public ClientsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        _vgParent = parent;
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.clinic_list_content, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ClientsAdapter.ViewHolder holder, int position) {
        holder._Clinic = _content.getClinic(position);
        holder._clinicName.setText(_content.getClinic(position)._name);
        holder._distance.setText(String.valueOf(_content.getClinic(position)._location._locationDistance));
        holder._isPreferred.setText(String.valueOf(_content.getClinic(position)._preferred));

        // Want to refactor this out to Controller if wanting to separate View from Control more.
        holder._View.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_bTwoPane) {
                    // Could make Clinic parcelable and pass it to arguments.
                    Bundle arguments = new Bundle();
                    arguments.putString(clinicDetailFragment.ARG_ITEM_ID, holder._Clinic._id);
                    arguments.putString(clinicDetailFragment.ARG_CLINIC_NAME, holder._Clinic._name);
                    arguments.putString(clinicDetailFragment.ARG_CLINIC_ADDR_STREET, holder._Clinic._location._streetName);
                    arguments.putString(clinicDetailFragment.ARG_CLINIC_ADDR_CITY, holder._Clinic._location._city);
                    arguments.putString(clinicDetailFragment.ARG_CLINIC_ADDR_STATE, holder._Clinic._location._stateCode);
                    arguments.putString(clinicDetailFragment.ARG_CLINIC_ADDR_POSTAL_CODE, holder._Clinic._location._postalCode);
                    arguments.putDouble(clinicDetailFragment.ARG_CLINIC_DISTANCE, holder._Clinic._location._locationDistance);
                    arguments.putString(clinicDetailFragment.ARG_CLINIC_PREFERRED, holder._Clinic._preferred.toString());
                    arguments.putDouble(clinicDetailFragment.ARG_CLINIC_GEOLOC_LATITUDE, holder._Clinic._geoLocation._latitude);
                    arguments.putDouble(clinicDetailFragment.ARG_CLINIC_GEOLOC_LONGITUDE, holder._Clinic._geoLocation._longitude);

                    clinicDetailFragment fragment = new clinicDetailFragment();
                    fragment.setArguments(arguments);

                    final Context context = _vgParent.getContext();
                    FragmentManager fm = ((AppCompatActivity)context).getSupportFragmentManager();
                    fm.beginTransaction()
                            .replace(R.id.clinic_detail_container, fragment)
                            .commit();
                } else {
                    Context context = v.getContext();
                    Intent intent = new Intent(context, clinicDetailActivity.class);
                    intent.putExtra(clinicDetailFragment.ARG_ITEM_ID, holder._Clinic._id);

                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return(this._content.getClinicCount());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View _View;
        public final TextView _isPreferred;
        public final TextView _clinicName;
        public final TextView _distance;
        public ClinicContent _Clinic;

        public ViewHolder(View view) {
            super(view);
            _View = view;
            _clinicName = (TextView) view.findViewById(R.id.clinic_name);
            _distance = (TextView) view.findViewById(R.id.distance);
            _isPreferred = (TextView) view.findViewById(R.id.preferred);
        }
    }

}
