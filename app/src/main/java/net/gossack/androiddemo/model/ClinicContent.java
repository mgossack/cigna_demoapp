package net.gossack.androiddemo.model;

/**
 * Created by Marc on 8/12/2016.
 */
/**
 * ClinicContent
 * Note: If objects of this class were used extensively would put this class in own file and/or expand
 * out interface (eg. getters/setters, helpers) to protect and for ease of use.
 */
public class ClinicContent {
    public String _id;
    public String _name;
    public Boolean _preferred;

    public class Location {
        public String _streetName;
        public String _city;
        public String _stateCode;
        public String _postalCode;
        public Double _locationDistance;
    };
    public Location _location;

    public class GeoLocation {
        public Double _longitude;
        public Double _latitude;
    };
    public GeoLocation _geoLocation;

    public ClinicContent() {
        _location = new Location();
        _geoLocation = new GeoLocation();
    }
    // Would normally privatize the data and create getters/setters.
}
