package net.gossack.androiddemo.model;

/**
 * Created by Marc on 8/11/2016.
 */

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

class ExceptionNoCurrentClinic extends Exception{
    ExceptionNoCurrentClinic(String s){
        super(s);
    }
}


/**
 * @class ClinicListContent class.
 */
public class ClinicListContent {
    JSONArray raw = null;
    JSONObject  rawObj = null;
    JSONArray _clinics;
    JSONObject _clinic;
    ArrayList<ClinicContent> _alistClinics = new ArrayList<ClinicContent>();

    ClinicListContent(String json_in) {
        try {
            raw = new JSONArray(json_in);
            rawObj = raw.getJSONObject(0);
        } catch (JSONException e) {
            Log.e(getClass().getSimpleName(),"Exception loading JSON", e);
        }
        _clinics = rawObj.optJSONArray("recommendations");

        // For smallish set of data read into Array of object so we can stop parsing JSON
        // This could also be a refactor point where raw input (HTTP JSON) updates to a local database.
        convertJSONInputToLocalStorage(_clinics);
    }

    void convertJSONInputToLocalStorage(JSONArray clinics)
    {
        if(_alistClinics.size() == 0 ) {    // Just allow the once for now...could be an compare and update/insert.
            for (int i = 0; i < clinics.length(); i++) {
                _alistClinics.add(getClinicObject(i));
            }
        }
    }

    public int getClinicCount() {
        return(_alistClinics.size());
    }

    // Sanity checks are needed
    public ClinicContent getClinic(int position) {
        if( position < _alistClinics.size() ) {
            return _alistClinics.get(position);
        }
        else {
            return null;
        }
    }

    public Long getClinicId(int position) {
        if( position < _alistClinics.size() ) {
            return Long.parseLong(_alistClinics.get(position)._id);
        } else {
            return -1L;
        }
    }

    private void setCurrentClinic(int position) throws ExceptionNoCurrentClinic
    {
        _clinic = getClinicObj(position);
        if(_clinic == null)
        {
            throw new ExceptionNoCurrentClinic("No current clinic is selected.");
        }
    }

    /**
     * getClinicObject()
     * @param position
     * @return ClinicContent object (basic interface for lack of more time)
     */
    private ClinicContent getClinicObject(int position) {
        // Populate the clinic object
        try {
            setCurrentClinic(position);

            ClinicContent clinic = new ClinicContent();
            clinic._id = getCurrentClinicId();
            clinic._name = getCurrentClinicName();
            clinic._preferred = isCurrentClinicPreferred();
            clinic._location._city = getCurrentClinicLocationCity();
            clinic._location._streetName = getCurrentClinicLocationStreet();
            clinic._location._postalCode = getCurrentClinicLocationPostalCode();
            clinic._location._stateCode = getCurrentClinicLocationStateCd();
            clinic._location._locationDistance = getCurrentClinicLocationDistance();
            clinic._geoLocation._latitude = getCurrentClinicLocationLatitude();
            clinic._geoLocation._longitude = getCurrentClinicLocationLongitude();

            return clinic;
        } catch(ExceptionNoCurrentClinic e) {
            Log.e(getClass().getSimpleName(), "No current Clinic set.");
        }
        return null;
    }

    /**
     * getClinicObj()
     * @param position
     * @return
     *
     */
    private JSONObject getClinicObj(int position) {
        JSONObject clinic = _clinics.optJSONObject(position);
        return clinic;
    }


    /**
     * getCurrentClinicId()
     * @return Id of currently selected clinic
     * @throws ExceptionNoCurrentClinic
     */
    private String getCurrentClinicId() throws ExceptionNoCurrentClinic
    {
        try {
            return (_clinic.optString("id"));
        } catch (Exception e) {
            throw new ExceptionNoCurrentClinic("No current clinic is selected.");
        }
    }

    /**
     * getCurrentClinicName
     * @return Name of currently selected clinic
     * @throws ExceptionNoCurrentClinic
     */
    private String getCurrentClinicName()  throws ExceptionNoCurrentClinic
    {
        try {
            return (_clinic.optString("name"));
        } catch (Exception e) {
            throw new ExceptionNoCurrentClinic("No current clinic is selected.");
        }
    }

    /**
     *
     * @return
     * @throws ExceptionNoCurrentClinic
     */
    Boolean isCurrentClinicPreferred()   throws ExceptionNoCurrentClinic
    {
        try {
            return(_clinic.optBoolean("preferred"));
        } catch (Exception e) {
            throw new ExceptionNoCurrentClinic("No current clinic is selected.");
        }
    }


    // The following need some sanity checks for real world chaos!
    // Individual calls can do additional parsing checks and corrections as is often needed
    // with test/JSON
    JSONObject getCurrentClinicLocation() throws ExceptionNoCurrentClinic
    {
        try {
            return (_clinic.optJSONObject("location"));
        } catch (Exception e) {
            throw new ExceptionNoCurrentClinic("No current clinic is selected.");
        }
    }

    String getCurrentClinicLocationStreet() throws ExceptionNoCurrentClinic {
        try {
            return getCurrentClinicLocation().optString("streetName");
        } catch (Exception e) {
            throw new ExceptionNoCurrentClinic("No current clinic is selected.");
        }
    }

    String getCurrentClinicLocationCity() throws ExceptionNoCurrentClinic {
        try {
            return getCurrentClinicLocation().optString("city");
        } catch (Exception e) {
            throw new ExceptionNoCurrentClinic("No current clinic is selected.");
        }
    }

    String getCurrentClinicLocationStateCd() throws ExceptionNoCurrentClinic {
        try {
            return getCurrentClinicLocation().optString("stateCode");
        } catch (Exception e) {
            throw new ExceptionNoCurrentClinic("No current clinic is selected.");
        }
    }

    String getCurrentClinicLocationPostalCode() throws ExceptionNoCurrentClinic {
        try {
            return getCurrentClinicLocation().optString("postalCode");
        } catch (Exception e) {
            throw new ExceptionNoCurrentClinic("No current clinic is selected.");
        }
    }

    Double getCurrentClinicLocationDistance() throws ExceptionNoCurrentClinic {
        try {
            return getCurrentClinicLocation().optDouble("locationDistance");
        } catch (Exception e) {
            throw new ExceptionNoCurrentClinic("No current clinic is selected.");
        }
    }

    JSONObject getCurrentClinicLocationGeoLocation() throws ExceptionNoCurrentClinic {
        try {
            return getCurrentClinicLocation().optJSONObject("geoLocation");
        } catch (Exception e) {
            throw new ExceptionNoCurrentClinic("No current clinic is selected.");
        }
    }

    Double getCurrentClinicLocationLongitude() throws ExceptionNoCurrentClinic {
        try {
            return getCurrentClinicLocationGeoLocation().optDouble("longitude");
        } catch (Exception e) {
            throw new ExceptionNoCurrentClinic("No current clinic is selected.");
        }
    }

    Double getCurrentClinicLocationLatitude() throws ExceptionNoCurrentClinic {
        try {
            return getCurrentClinicLocationGeoLocation().optDouble("latitude");
        } catch (Exception e) {
            throw new ExceptionNoCurrentClinic("No current clinic is selected.");
        }
    }
}