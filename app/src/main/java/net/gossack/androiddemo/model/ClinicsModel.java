package net.gossack.androiddemo.model;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ClinicsModel.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ClinicsModel#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ClinicsModel extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ClinicListContent contentClinics;                 // Our content object to be read in below
    private ClinicsModel.LoadClinicListTask contentClinicsTask = null;  // of AsyncTask type below

    private OnClinicModelFragmentInteractionListener _Listener;

    public ClinicsModel() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ClinicsModel.
     */
    // TODO: Rename and change types and number of parameters
    public static ClinicsModel newInstance(String param1, String param2) {
        ClinicsModel fragment = new ClinicsModel();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    //(called on configuration changes)
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);    // So we hold onto the data once it's loaded.
        retrieveModel();            // Get the data
    }

    // retrieveModel() either the content is loaded and we pass it to the activity,
    // or we load it from the source in a background thread (AsyncTask)
    synchronized private void retrieveModel() {
        if (contentClinics != null) {
            _Listener.setupClinicsList(contentClinics);     // Should be the main activity or where this is embedded...
        }
        else {
            if (contentClinics == null && contentClinicsTask == null) {
                contentClinicsTask = new LoadClinicListTask();      // new task to be kicked off in background.
                executeBackgroundTask(contentClinicsTask, getActivity().getApplicationContext());   // kick it off
            }
        }
    }

    /**
     * executeBackgroundTask - Example: Paying attention to the build version if working with some old OS versions.
     * @param task
     * @param params
     * @param <T>
     */
    @TargetApi(11)
    static public <T> void executeBackgroundTask(AsyncTask<T, ?, ?> task, T... params)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
        }
        else {
            task.execute(params);
        }
    }

    private class LoadClinicListTask extends AsyncTask<Context, Void, Void>
    {
        private ClinicListContent clinicsLocal = null;
        private Exception e=null;

        @Override
        protected Void doInBackground(Context... ctxt) {
            try {
                StringBuilder buf=new StringBuilder();
                InputStream json=ctxt[0].getAssets().open("clinicList.json");
                BufferedReader in=new BufferedReader(new InputStreamReader(json));
                String str;
                while((str = in.readLine()) != null) {
                    buf.append(str);
                }
                in.close();
                clinicsLocal = new ClinicListContent(buf.toString());
            } catch(Exception e)
            { // Bad JSON file??
                this.e=e;
                Log.e(getClass().getSimpleName(), "Exception loading contents", e);
            }
            return(null);
        }

        // Called on the main application thread with nothing else going on so we are okay to update.
        @Override
        public void onPostExecute(Void arg0) {
            if (e == null) {
                ClinicsModel.this.contentClinics = clinicsLocal;
                ClinicsModel.this.contentClinicsTask = null;
                retrieveModel();    // Call retrieveModel again... only this time there should be content to pass to the parent activity.
            } else {
                Log.e(getClass().getSimpleName(), "Exception loading contents", e);
            }
        }
    }


//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        TextView textView = new TextView(getActivity());
//        textView.setText(R.string.hello_blank_fragment);
//        return textView;
//    }

//    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (_Listener != null) {
//            _Listener.onFragmentInteraction(uri);
//        }
//    }

    public void setContext(Context context) {
        if (context instanceof OnClinicModelFragmentInteractionListener) {
            _Listener = (OnClinicModelFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        _Listener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * Added callback to be implemented so the setupClinicsList is guaranteed to exist
     *  and used above.
     */
    public interface OnClinicModelFragmentInteractionListener
    {
        void setupClinicsList(ClinicListContent clinicsList);
    }
}
