package net.gossack.androiddemo;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;


import net.gossack.androiddemo.adapter.ClientsAdapter;
import net.gossack.androiddemo.model.ClinicListContent;
import net.gossack.androiddemo.model.ClinicsModel;

import java.util.List;

/**
 * An activity representing a list of clinics. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link clinicDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class clinicListActivity extends AppCompatActivity  implements ClinicsModel.OnClinicModelFragmentInteractionListener {
    private ClinicListContent _clinicList;
    private ClientsAdapter _adapter;
    private RecyclerView _rvListClinics;
    private static final String CLINICMODEL = "model";

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean _TwoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clinic_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        // Set the context into the Model only so the model has this as a listener controller.
        ClinicsModel frag = new ClinicsModel();
        frag.setContext(this);

        if (getFragmentManager().findFragmentByTag(CLINICMODEL) == null) {
            getFragmentManager().beginTransaction().add((Fragment)frag, CLINICMODEL).commit();
        }

        if (findViewById(R.id.clinic_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            _TwoPane = true;
        }

    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        _adapter.setTwoPane(_TwoPane);
        recyclerView.setAdapter(_adapter);   /// new SimpleItemRecyclerViewAdapter(DummyContent.ITEMS));
    }

    /**
     *  interface OnClinicModelFragmentInteractionListener.setupClinicsList()
     * @param clinicList
     * @note This acting as "controller" receives the clinics list from the model.
     * Could implement a separate controller class to help separate the data and provide for
     * easier use of data in multiple front ends/apps.
     */
    @Override
    public void setupClinicsList(ClinicListContent clinicList) {
        _clinicList = clinicList;

        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.clinic_list);
        assert recyclerView != null;
        // Set list information to ListViewAdapter
        _adapter = new ClientsAdapter(_clinicList);
        _adapter.setTwoPane(_TwoPane);  // Hack for lack of time to separate MVC
        recyclerView.setAdapter(_adapter); // new SimpleItemRecyclerViewAdapter(DummyContent.ITEMS));
    }

}
